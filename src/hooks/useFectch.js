import { useEffect, useRef, useState } from "react";

export const useFectch = (url) => {
  const isMounted = useRef(true);
  const [state, setState] = useState({
    data: null,
    loading: true,
    error: null,
  });

  useEffect(() => {
    return () => {
      isMounted.current = false;
    };
  }, []);

  useEffect(() => {
    setState({ data: null, loading: true, error: null });
    fetch(url)
      .then((resp) => resp.json())
      .then((data) => {
         if (isMounted.current) {
                setState({
                error: null,
                loading: false,
                data,
              });
            }
        
      });
  }, [url]);
  // quiero que solamente se ejecute el use useEffect cuando el url cambie

  return state;
};
