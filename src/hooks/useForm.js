import { useState } from "react";



export const useForm = (initialState = {}) => {
  // el initialState seria un objeto igual al useState de FormWithCustomHook 
  // lo declaramos como objeto para que no reviente

  const [values, setValues] = useState(initialState);

  const reset =()=>{

    setValues(initialState);
  }

  const handleInputChange = ({ target }) => {
    
    setValues({
      ...values,
      [target.name]: target.value,
    });
  };

  return [values, handleInputChange, reset];
}

