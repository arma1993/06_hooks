import React from 'react';
import ReactDOM from "react-dom/client";
import { TodoApp } from './components/08-useReducer/TodoApp';
// import { TodoApp } from './components/08-useReducer/TodoApp';
// import { Padre } from './components/07-tarea-memo/Padre';
// import { CallbackHook } from './components/06-memos/CallbackHook';
// import { MemoHook } from './components/06-memos/MemoHook';
// import { Memorize } from './components/06-memos/Memorize';
// import { Layout } from './components/05-useLayoutEffect/Layout';
// import { RealExampleRef } from './components/useRef/RealExampleRef';
// import { FocusScreen } from './components/useRef/FocusScreen';
// import { MultipleCustomHooks } from './components/03-examples/MultipleCustomHooks';
// import { FormWithCustomHook } from './components/02-useEffect.js/FormWithCustomHook';
// import { SimpleForm } from './components/02-useEffect.js/SimpleForm';
// import { CounterWithCustomHook } from './components/01-useState/CounterWithCustomHook';
// import { CounterApp } from './components/01-useState/CounterApp';
// import { HookApp } from './HookApp';




ReactDOM.createRoot(document.querySelector("#root")).render(
    < TodoApp/>
);
// es la unica manera de que no salte el warning tras la actualización al react 18 


