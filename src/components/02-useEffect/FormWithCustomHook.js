import React, { useEffect } from "react";
import { useForm } from "../../hooks/useForm";
import "./effects.css";


export const FormWithCustomHook = () => {
//---------------------------------------------------------------------------------------------------------------------------------------------------->

//   const [formState, setFormState] = useState({
//     name: "",
//     email: "",
//     password: ""
//   });
//   const { name, email, password } = formState; //usamos desestructuracion de un objeto
//---------------------------------------------------------------------------------------------------------------------------------------------------->

const [formValues, handleInputChange] = useForm({
    name: "",
    email: "",
    password: ""
  });
  const { name, email, password } = formValues;

  useEffect(() => {
    console.log("email cambio")
    
  },[email]);
  

// esta seria otra manera de hacer un login de formulario de name email y password codigo mas limpio
//---------------------------------------------------------------------------------------------------------------------------------------------------->

  
//---------------------------------------------------------------------------------------------------------------------------------------------------->
    const handleSubmit = (e)  =>{
        e.preventDefault();
        console.log(formValues)
    }
return (
    <form onSubmit={handleSubmit}>
      <h1>FormWithCustomHook</h1>
      <div>
        <input
          type="text"
          name="name"
          className="form-control"
          placeholder="Tu nombre"
          autoComplete="off"
          value={name}
          onChange={handleInputChange}
        />
      </div>

      <div>
        <input
          type="text"
          name="email"
          className="form-control"
          placeholder="Email"
          autoComplete="off"
          value={email}
          onChange={handleInputChange}
        />
      </div>

      {/* creamos un input de password 
      
      en este componente creamos un apartado con su password ylo mandamos a desestructurar en el useState
      creamos un customhook que se llama useForm.js 
      */}
      <div>
        <input
          type="passwor"
          name="password"
          className="form-control"
          placeholder="******"
          autoComplete="off"
          value={ password }
          onChange={handleInputChange}
        />
      </div>
      <button type="submit" className="btn btn-primary">
          Guardar
      </button>
      </form>
  );
};
