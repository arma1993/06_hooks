import React, { useEffect, useState } from "react";
import "./effects.css";
import { Message } from "./Message";

export const SimpleForm = () => {
//---------------------------------------------------------------------------------------------------------------------------------------------------->

  const [formState, setFormState] = useState({
    name: "",
    email: "",
  });
  const { name, email } = formState; //usamos desestructuracion de un objeto
//---------------------------------------------------------------------------------------------------------------------------------------------------->

  useEffect(() => {
    console.log("hey");
  }, []); // este seria el general cambiaria en las dos formas
  //---------------------------------------------------------------------------------------------------------------------------------------------------->
  useEffect(() => {
    // console.log("email cambio");
  }, [email]);
  //---------------------------------------------------------------------------------------------------------------------------------------------------->
  useEffect(() => {
    // console.log("name change");
  }, [name]);
  // podemos usar tantas veces querramos el useEffect para que solo cambie donde escribimos tal y como muestra en el codigo del arreglo []
  //---------------------------------------------------------------------------------------------------------------------------------------------------->

  const handleInputChange = ({ target }) => {
    setFormState({
      ...formState,
      [target.name]: target.value,
    });
  };
  //---------------------------------------------------------------------------------------------------------------------------------------------------->
  return (
    <>
      <h1>useEffect</h1>
      <div>
        <input
          type="text"
          name="name"
          className="form-control"
          placeholder="Tu nombre"
          autoComplete="off"
          value={name}
          onChange={handleInputChange}
        />
      </div>

      <div>
        <input
          type="text"
          name="email"
          className="form-control"
          placeholder="Email"
          autoComplete="off"
          value={email}
          onChange={handleInputChange}
        />
      </div>

      {(name === '123') && <Message/>} 
    </>
  );
};
