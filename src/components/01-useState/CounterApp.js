import React from "react";
import "./counter.css";
import { useState } from "react";

export const CounterApp = () => {
  const [state, setState] = useState({
    counter1: 10,
    counter2: 20,
    counter3: 30,
    counter4: 40,
  });
  const { counter1, counter2 } = state;

  return (
    <>
      <h1> Counter {counter1} </h1>
      <h1> Counter {counter2} </h1>

      <hr />

      <button
        className="btn btn-primary"
        onClick={() => {
          setState({
            ...state, // spread operator lo hacemos para que no cambie el valor de ningun valor del state y solo se ejecute el que mandemos a usar
            counter1: counter1 + 1,
          });
        }}
      >
        +1
      </button>
    </>
  );
};
