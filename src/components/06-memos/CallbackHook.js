import React, { useCallback, useState } from 'react'
import "../02-useEffect/effects.css";
import { ShowIncrement } from './ShowIncrement';


export const CallbackHook = () => {

    
    const [counter, setCounter] = useState( 10 )

    // const increment =  ()=>{
    //         setCounter( counter + 1 )
    // }

const increment = useCallback(( num ) => { 
    // lo recibimos ese 5 como si fuera cualquier otro argumento en este caso se llama num
    
        setCounter( c => c + num ) 
        // lo llamamos de nuevo en el setcounter el num
        // con el c => c eliminamos la dependecia del counter y por lo tanto no me lo vuelve a cargar el me volvi a generar
},[setCounter])


  return (
    
    <>
    <h1> Callback Hook: { counter } </h1>
    <hr/>

    <ShowIncrement increment={increment}/> 
    {/* llamamos del componente padra al hijo desde aca  */}

    
    </>
  )
}
