import React from "react";

export const Small = React.memo(({ value }) => {
  console.log("Me volvi a llamar :(");

  return <small>{value}</small>;
});

//memo:
// es una funcion que va memorizar la forma organizada de mi componente y solo se disparara
// si las propertyes cambian
