import React from 'react'

// como vemos llamamos al padre desde aca con el incremete y la desestructuracion

export const ShowIncrement = React.memo(({increment}) => { 


    console.log('me volvi a generar :(')
  return (
    <button
    className='btn btn-primary'
    onClick={ () => {
        increment( 5 );
        // llama esta funcion y lo incrementamos en 5 
    }}
    >
        increment
    </button>
  )
})
