

const initialState = [
    {
        id: 1, 
        todo: 'comprar pan',
        done: false,
    }
];


const todoReducer =(state = initialState, action)=>{

    if (action?.type === 'agregar'){
        // si ? action tiene algun valor lee el type si no, no me leas nada con eso lo hacemos con el signo de interrogacion
        return [...state, action.payload];
    }
    return state;

}

let todos = todoReducer();


const newTodo= {

    id: 1, 
    todo: 'comprar leche',
    done: false,
}


const agregarTodoAction ={
    type: 'agregar',
    payload: newTodo
}

todos = todoReducer( todos, agregarTodoAction );
console.log(todos);


// no hacer push si se trabaja en react por que el push modifica todo el objeto 
