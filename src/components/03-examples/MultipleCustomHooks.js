import { useCounter } from "../../hooks/useCounter";
import { useFectch } from "../../hooks/useFectch";

import "../02-useEffect/effects.css";

export const MultipleCustomHooks = () => {
  
  const { counter, increment } = useCounter(1);

  const { loading, data } = useFectch(
    `https://www.breakingbadapi.com/api/quotes/${counter}`
  );
  const { author, quote } = (!!data && data[0]) || (!!data && {quote:'No se encontro nada por favor pase a la siguiente fase',author:'not found'});;

  //   si data es cierto !! entonces&& ejecutame data de lo contrario no me ejecutes

  console.log(author, quote);

  return (
    <div>
      <h1>Breaking Bad Quotes</h1>

      <hr />

      {
        // es una codicion ternaria
        loading ? (
          <div className="alert alert-info text-center">loading...</div>
        ) : (
          <blockquote className="blockquote text-right">
            <p className="mb-0"> {quote}</p>
            <hr />
            <footer className="blockquote-footer "> {author} </footer>
          </blockquote>
        )
      }
      <button className="btn btn-primary" onClick={increment}>
        Siguiente Frase
      </button>
    </div>
  );
};
