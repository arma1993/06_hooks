import { useLayoutEffect, useRef, useState } from "react";
import { useCounter } from "../../hooks/useCounter";
import { useFectch } from "../../hooks/useFectch";

import "./layout.css"

export const Layout = () => {
  
  const { counter, increment } = useCounter(1);

  const { data } = useFectch(
    `https://www.breakingbadapi.com/api/quotes/${counter}`
  );
  const {  quote } = (!!data && data[0]) || (!!data && {quote:'No se encontro nada por favor pase a la siguiente fase',author:'not found'});;

  //   si data es cierto !! entonces&& ejecutame data de lo contrario no me ejecutes
    const pTag = useRef();
    const [boxSize, setBoxSize] = useState({}) ; 
  useLayoutEffect(()=>{
    setBoxSize(pTag.current.getBoundingClientRect());
    },[quote])

  return (
    <div>
      <h1>LayoutEffects</h1>

      <hr />
        <blockquote className="blockquote text-right">
            <p className="mb-0"
            ref={pTag}
            > 
            {quote}
            </p>
            <hr />
        </blockquote>
         <pre>
             {JSON.stringify(boxSize, null, 3)}

      </pre>
      <button className="btn btn-primary" onClick={increment}>
        Siguiente Frase
      </button>
    </div>
  );
};
