import { useState } from "react"
import "../02-useEffect/effects.css"
import { MultipleCustomHooks } from "../03-examples/MultipleCustomHooks"


export const RealExampleRef = () => {
    
    const [show, setShow] = useState(false)
  
    return (
    <div><h1>RealExampleRef</h1>
    <hr/>

    
    {show && < MultipleCustomHooks/>}  
   
   
   {/* si show && esta en true entonces muestramelo  en caso contrario ocultamelo*/}

            <button 
            className="btn btn-primary mt-4"
            onClick={() => {
                setShow (!show);
                // set show es el valor opuesto a show
            }}
            >
                Show/Hide
            </button>
    </div>
  )

}
