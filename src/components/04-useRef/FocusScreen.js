import React, { useRef } from 'react'

import "../02-useEffect/effects.css"
export const FocusScreen = () => {
  
    const inputRef = useRef();
    // console.log(ref)
  
    //----------------------------------------------------------------------------------
    const handleClick=()=>{

        // document.querySelector('input').select(); // esto hace que el focus no se vaya a la hora de ejecutar el boton
        inputRef.current.select(); // es otra manera de hacerla mas corta con el useRef
    }
    //----------------------------------------------------------------------------------

    
    return (
    <div><h1>FocusScreen</h1>
    <hr/>

    <input
    ref={inputRef}
    className='form-control'
    placeholder='Su nombre'
    />

    <button className='btn btn-primary mt-3'
        onClick={handleClick}
    
    >
        Focus
    </button>
    
    
    </div>
  )
}
